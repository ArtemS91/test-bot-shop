const {Scenes,session }=require('telegraf')
const bot =require('./bot/connection/token.bot')
const womanWizard =require('./bot/scene/woman.scene')

////connection
require('./bot/connection/local.bot')

// ////i18n
// require('./bot/connection/i18t.bot')

// ///en
// require('./bot/command/commandsFromMenu/en.commandsFromMenu')


// ////ua
// require('./bot/command/commandsFromMenu/ua.commandsFromMenu')

////start 
require('./bot/command/start.command')




///menu of commands
require('./bot/command/setCommands/setCommands')


////about
require('./bot/command/commandsFromMenu/about.commandsFromMenu')

////contacts
require('./bot/command/commandsFromMenu/contacts.commandsFromMenu')

///location
require('./bot/command/commandsFromMenu/location.commandsFromMenu')

////back
require('./bot/command/commandsFromMenu/back.commandsFromMenu')

///faq
require('./bot/command/commandsFromMenu/faq.commandsFromMenu')



////scene connect
const stage = new Scenes.Stage([womanWizard]);
bot.use(session());
bot.use(stage.middleware());

////commands
require('./bot/command/on/woman/woman.on.command')
require('./bot/command/on/approve/approve.on')
require('./bot/command/on/cardPay/cardPay.on')
require('./bot/command/on/afterSendPay/afterSendPay.on')






