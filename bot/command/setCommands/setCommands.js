const bot=require('../../connection/token.bot')

module.exports=bot.telegram.setMyCommands([
    {
        command: "/about",
        description: "🔎Дізнатися хто ми"
    },
    {
        command: "/faq",
        description: "🗒️Відповіді на часто задаваїмі питання"
    },
    {
        command: "/contacts",
        description: "☎️ Наші контакти"
    },
    {
        command: "/ua",
        description: "🇺🇦Українська мова"
    },
    {
        command: "/en",
        description: "🇺🇸Англійська мова"
    },
    {
        command: "/back",
        description: "🔙 Повернутися на початок"
    },
    {
        command: "/location",
        description: "📡Наша локація"
    }
])
