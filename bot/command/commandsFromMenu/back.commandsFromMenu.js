const bot = require("../../connection/token.bot");
const personButton = require("../../keyboard/person.keyboard");

bot.command("back", async (ctx) => {
  // const description = ctx.i18n.t("start.description");
  // const questing = ctx.i18n.t("start.questing");

  // console.log(ctx.i18n.locale());
  try {
    await ctx.replyWithMediaGroup([
      {
        type: "photo",
        media:
          "https://res.cloudinary.com/dllwyhvzg/image/upload/f_auto,q_auto/arwptwzdttghw7tkn2av",
        caption: "Вітаємо вас в нашому магазині взуття. Ми впевненні , що вам у нас сподобається.",
        caption_entities: "Вітаємо вас в нашому магазині взуття. Ми впевненні , що вам у нас сподобається.",
      },
    ]);
    return ctx.reply("Для кого вас цікавить взуття ? 🥾", personButton);
  } catch (erro) {
    console.log(erro);
  }
});
