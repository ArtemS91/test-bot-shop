const choose = require("../keyboard/woman.chosse.keyboard");
const size = require("../keyboard/size.keyboard");
const checkingData = require("../keyboard/approveDataWoman.keyboard");
const { Scenes, Composer } = require("telegraf");


const itemsWomanPhotos = new Composer();
itemsWomanPhotos.action('woman', async (ctx) => {
  ctx.wizard.state.data = {};

  await ctx.replyWithMediaGroup([
    {
      type: "photo",
      media:
        "https://res.cloudinary.com/dllwyhvzg/image/upload/f_auto,q_auto/v1/woman/yxbfvsqj4f28iroyzrjd",
      caption:
        "Код товару:Archi. Це модель Archi. Гарне взуття яке підійде для будьякого твого іміджу. Зручне, комфортне і легке",
    },
    {
      type: "photo",
      media:
        "https://res.cloudinary.com/dllwyhvzg/image/upload/f_auto,q_auto/v1/woman/i7yrxanzu2yd3vv3uce0",
      caption:
        "Код товару:Semmy. Це модель Semmy. Гарне взуття яке підійде для будьякого твого іміджу. Зручне, комфортне і легке",
    },
    {
      type: "photo",
      media:
        "https://res.cloudinary.com/dllwyhvzg/image/upload/f_auto,q_auto/v1/woman/zisw4ikeicjb1dcy6wr0",
      caption:
        "Код товару:Angela. Це модель Angela. Гарне взуття яке підійде для будьякого твого іміджу. Зручне, комфортне і легке",
    },
    {
      type: "photo",
      media:
        "https://res.cloudinary.com/dllwyhvzg/image/upload/f_auto,q_auto/v1/woman/x68jrmsrlzztayzkssla",
      caption:
        "Код товару:Free. Це модель Free. Гарне взуття яке підійде для будьякого твого іміджу. Зручне, комфортне і легке",
    },
  ],choose);
  // await ctx.reply("Що ти обираєш? 🟢", choose);

  return ctx.wizard.next();
});
const itemsWomanSize = new Composer();
itemsWomanSize.on('text', async (ctx) => {
  ctx.wizard.state.data.item = ctx.message.text;
  if (ctx.message.text == "Archi") {
    ctx.wizard.state.data.price = "2400грн";
    await ctx.replyWithHTML(
      `Модель Archi має дуже привабливу ціну <b>2400грн</b>.На данний час доступны наступны розмыри.Який вам потрібен,`,
      size
    );
  }
  return ctx.wizard.next();
});

const getName = new Composer();
getName.on("text", async (ctx) => {
  ctx.wizard.state.data.size = ctx.message.text;
  await ctx.reply("введіть Прізвище і Імя отримувача ");
  return ctx.wizard.next();
});

const getNumber = new Composer();
getNumber.on("text", async (ctx) => {
  ctx.wizard.state.data.name = ctx.message.text;
  await ctx.reply("введіть номер телефону ");
  return ctx.wizard.next();
});

const getPostDepartment = new Composer();
getPostDepartment.on("text", async (ctx) => {
  ctx.wizard.state.data.phone = ctx.message.text;
  await ctx.reply("введіть мітсо і номер відділення Нової Пошти ");
  return ctx.wizard.next();
});

const checkData = new Composer();
checkData.on("text", async (ctx) => {
  ctx.wizard.state.data.postDepartment = ctx.message.text;
  await ctx.reply("давай перевіремо данні по замовленню і відправленню. ✅");
  await ctx.replyWithHTML(`
<b>Замовлення Товару</b>
<b>Товар</b>:   ${ctx.wizard.state.data.item}
<b>Розмір</b>:  ${ctx.wizard.state.data.size}
<b>Ціна</b>:    ${ctx.wizard.state.data.price}
<b>Данні клієнта</b> 
<b>Прізвище і імя</b>:   ${ctx.wizard.state.data.name}
<b>Номер відділення</b>: ${ctx.wizard.state.data.postDepartment} 
<b>Номер телефону</b>:   ${ctx.wizard.state.data.phone}
    `);
  await ctx.reply("Ми все вірно записали?✍️", checkingData);
  return await ctx.scene.leave();
});


const womanWizard = new Scenes.WizardScene(
  "woman-scene",
  itemsWomanPhotos,
  itemsWomanSize,
  getName,
  getNumber,
  getPostDepartment,
  checkData,
);

module.exports = womanWizard;
