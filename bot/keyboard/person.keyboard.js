const {Markup} =require('telegraf')

module.exports=Markup.inlineKeyboard([
        Markup.button.callback('для жінок 💁‍♀️', 'woman'),
        Markup.button.callback('для чоловіків 🙎‍♂️', 'man'),
        Markup.button.callback('для дітей 👨‍👦‍👦', 'child'),
    ], {columns:2})