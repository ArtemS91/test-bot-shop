const {Markup} =require('telegraf')

module.exports=Markup.inlineKeyboard([
    Markup.button.callback('Картою зараз тут 💳 ', 'now'),
    Markup.button.callback('При отриманні на пошті 🏪', 'later'),
  
], {columns:1})